import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AuthService } from '../shared/services/auth.service';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    constructor(public router: Router, private loginUser: AuthService, private toastr: ToastrService) {}
    loginDetail = {};
    icon_show = false;
    
    onLoggedin() {
        this.icon_show = true;
        this.loginUser.loginUsers(this.loginDetail).subscribe
        (
            res => {
                if(res.StatusCode === 200 ){
                    console.log(res.Data);
                    this.icon_show = false;
                    // localStorage.setItem('token', JSON.stringify({ token: res.Data.token, role: res.Data.role }));
                    localStorage.setItem('token', res.Data.token);
                    localStorage.setItem('roles', res.Data.role);
                    localStorage.setItem('user_name', res.Data.fname);
                     let role = res.Data.role;
                    switch(role) {
                        case "A":
                            this.router.navigate(['/user']);
                        break;
                        case "SA":
                        this.router.navigate(['/product']);
                        break;

                    }
                    // if (res.Data.role == "A") {
                    //     this.router.navigate(['/user']);
                    // }
                    // else if (res.Data.role == "SA") {
                    //     this.router.navigate(['/product']);
                    // }
                    
                    this.toastr.success(res.message, 'Success');
                } else if (res.StatusCode == 404) {
                    this.toastr.error(res.message, 'Warning');
                }
               
            },
            err => {
                this.icon_show = false;
                this.toastr.error(err.error.message, 'Warning'); 
            }
        )
        
    }
    ngOnInit() {
       
    }
    

    
}
