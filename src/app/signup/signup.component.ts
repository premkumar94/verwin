import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    registerDetail = {}
    user:any = []
    icon_show = false;
    constructor(private register: AuthService, private router: Router) {}
    registerUser() {
        this.icon_show = true;
        this.register.registerUser(this.registerDetail).subscribe(
            res => {
                if(res.StatusCode == 201) {
                    this.icon_show = false;
                    this.router.navigate ( [ '/login' ] );
                }
                else if (res.StatusCode == 404) {
                    alert('Again Register');
                }
            }
        )
    }
    ngOnInit() {
        
    }
    
}
