import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { UserService } from '../../../shared/services/user.service'
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  animations: [routerTransition()]
})
export class AddComponent implements OnInit {
  userCreate = {}
  userRole = [
    {
      id : 'RU',
      role : 'Reg.User'
    },
    {
      id : 'I',
      role : 'Interviewer'
    },
    {
      id : 'E',
      role : 'Expert'
    },
    {
      id : 'CA',
      role : 'Content Administrator'
    },
    {
      id : 'SA',
      role : 'Super Admin'
    },
    {
      id : 'HD',
      role : 'Helpdesk'
    },
    {
      id : 'PM',
      role : 'Product Manager'
    },
    {
      id : 'O',
      role : 'Operations'
    }
  ];
 
//  url_lists = this.url_list.split('/');
  constructor(private userAddReceiveData: UserService, private toastr: ToastrService, private router: Router ,private route: ActivatedRoute) { 
    
    
  }
 
  
  icon_show = false;
  create () {
    this.icon_show = true;
    this.userAddReceiveData.userAddReceive(this.userCreate).subscribe(
      res => {
        console.log(res);
        if(res.StatusCode == 201) {
          this.icon_show = false;
          this.toastr.success(res.message, 'Success');
          this.router.navigate(['/user'])
        }
      },
      err => {
        this.toastr.error(err.error.message, 'Warning');
        this.icon_show = false;
      }
    )
  }
  ngOnInit() {
   
  }
  

}
