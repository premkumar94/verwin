import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { UserService } from '../../shared/services/user.service'
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  animations: [routerTransition()]
})
export class UserComponent implements OnDestroy, OnInit {
  allUsers: any = []
  dtOptions: DataTables.Settings = {};
  private datatableElement: DataTableDirective;
  dtTrigger = new Subject();
  constructor(private allUser: UserService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
          
        });
        return row;
      }
    };
    this.allUser.allUser().subscribe(
      res => {
        this.allUsers = res;
        this.dtTrigger.next();
      }
    )
    
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

}
