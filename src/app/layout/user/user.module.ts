import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { DataTablesModule } from 'angular-datatables';
import { UserComponent } from './user.component';
import { AddComponent } from './add/add.component';
import { UserService } from  '../../shared/services/user.service';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    DataTablesModule
  ],
  providers : [UserService],
  declarations: [UserComponent, AddComponent]
})
export class UserModule { }
