import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    urlArray;
    constructor(private router: Router) {
        this.router.events.subscribe(value => {
            this.urlArray = router.url.toString().split('/').splice(1, router.url.length);
        });
    }
    
 
    ngOnInit() {
       
       
    }
}
