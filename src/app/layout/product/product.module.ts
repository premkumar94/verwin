import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import { FormsModule } from '@angular/forms';
import { NgxEditorModule } from 'ngx-editor';

@NgModule({
  imports: [
    CommonModule,
    ProductRoutingModule,
    FormsModule,
    NgxEditorModule
  ],
  declarations: [ProductComponent]
})
export class ProductModule { }
