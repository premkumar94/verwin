import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import 'rxjs/add/operator/map'
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  animations: [routerTransition()]
})
export class ProductComponent implements OnInit {
  addProduct = {}
  constructor() { }

  ngOnInit() {
  }

}
