import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { TranslateDefaultParser } from '@ngx-translate/core';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private auth: AuthService ) {}

    canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): boolean {
        if (this.auth.isLogin() && this.auth.isLoggingIn() === "A") {
            
            return true;
            
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }

        
    }
   
}
