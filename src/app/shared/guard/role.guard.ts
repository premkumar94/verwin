import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(private router: Router, private auth: AuthService) {}
  canActivate()
    {
      if (this.auth.isLogin() && this.auth.isLoggingIn() === "SA") {
            
        return true;
    }
    else {
        this.router.navigate(['/login']);
        
        return false;
       
    }
  }
}
