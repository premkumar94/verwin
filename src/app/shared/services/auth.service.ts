import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'; 

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  
  public Url:any = "http://localhost:5000/UserRegistration";
  public logInUrl:any = "http://localhost:5000/Login";
  constructor(private http: HttpClient) { }
    registerUser(user) {
      return this.http.post<any>(this.Url, user);
    }
    loginUsers(user){
      return this.http.post<any>(this.logInUrl, user);
    }
    isLogin() {
      return !!localStorage.getItem('token');
     
    }
    getToken() {
      return localStorage.getItem('token')
    }
    isLoggingIn() {
      return localStorage.getItem('roles');
    }
    username(){
      return localStorage.getItem('user_name');
    }
}
