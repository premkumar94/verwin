import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'; 

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private allUserUrl = "https://jsonplaceholder.typicode.com/users"
  private userAddUrl = "http://localhost:5000/UserRegistrationGustUser"
  constructor(private http : HttpClient) { }
  allUser() {
    return this.http.get(this.allUserUrl);
  }
  userAddReceive(user) {
    return this.http.post<any>(this.userAddUrl, user);
  }
}
